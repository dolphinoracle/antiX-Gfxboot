# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# anticapitalista <anticapitalista@riseup.net>, 2021
# David Rebolo Magariños <drgaga345@gmail.com>, 2021
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-07-04 21:24+0200\n"
"PO-Revision-Date: 2021-07-09 16:40+0000\n"
"Last-Translator: David Rebolo Magariños <drgaga345@gmail.com>, 2021\n"
"Language-Team: Galician (Spain) (https://app.transifex.com/anticapitalista/teams/10162/gl_ES/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: gl_ES\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

msgid "Advanced Options"
msgstr "Opcións avanzadas"

msgid "Back to main menu"
msgstr "Volver ao menú principal"

msgid "Back to main menu (or press »ESC«)"
msgstr "Volva ao menú principal (ou prema »ESC«)"

msgid "Boot Process Performance Visualization"
msgstr "Visualización do rendemento do proceso de arranque"

msgid "Boot Rescue Menus"
msgstr "Menús de arranque de rescate"

msgid "Boot options"
msgstr "Opcións do arranque"

msgid "Change passwords before booting"
msgstr "Cambiar os contrasinais antes de iniciar"

msgid "Check LiveUSB and persistence ext2/3/4 file systems"
msgstr ""
"Verificar os sistemas de ficheiros ext2/3/4 do USV-executable e da área "
"persistente"

msgid "Check integrity of the live media"
msgstr "Verificar a integridade do dispositivo externo"

msgid "Console options"
msgstr "Opcións da consola"

msgid "Copy the compressed file system to RAM"
msgstr "Copiar o sistema de ficheiros comprimido para a RAM"

msgid "Custom"
msgstr "Personalizar"

msgid "Customize Boot (text menus)"
msgstr "Personalizar o arranque (texto dos menús)"

msgid "Desktop options"
msgstr "Opcións do escritorio"

msgid "Disable ACPI"
msgstr "Desactivar ACPI"

msgid "Disable GRUB theme"
msgstr "Desactivar o tema GRUB"

msgid "Disable Intel graphics invert"
msgstr "Desactivar a inversión de gráficos Intel"

msgid "Disable LiveUSB-Storage feature"
msgstr "Desactivar a función de almacenamento USB en directo"

msgid "Disable automount via fstab"
msgstr "Desactivar a montaxe automática mediante fstab"

msgid "Disable console width"
msgstr "Desactivar o ancho da consola"

msgid "Disable dual video card detection"
msgstr "Desactivar a detección da tarxeta de vídeo dual"

msgid "Disable swap"
msgstr "Desactivar trocas"

msgid "Disable text-splash screen"
msgstr "Desactivar a pantalla de presentación de texto"

msgid "Don't look for USB-2 devices"
msgstr "Non busques os dispositivos USB-2"

msgid "Don't save files across reboots"
msgstr "Non garde ficheiros durante os reinicios"

msgid "Don't set repositories based on timezone."
msgstr "Non configure repositorios baseados na zona horaria."

msgid "EFI Bootloader"
msgstr "Cargador do arranque EFI"

msgid "Enable EFI subsystem for RT-PREEMPT kernels"
msgstr "Activa o subsistema EFI para os núcleos RT-PREEMPT"

msgid "Enable GRUB theme"
msgstr "Active o tema GRUB"

msgid "Enable LiveUSB-Storage feature"
msgstr "Activa a función de almacenamento USB en directo"

msgid "Enable automount via fstab"
msgstr "Activar a montaxe automática mediante fstab"

msgid "Enable dual video card detection"
msgstr "Activar a detección da tarxeta de vídeo dual"

msgid "Enable text-splash screen"
msgstr "Activar a pantalla de presentación de texto"

msgid "Failsafe options"
msgstr "Opcións a proba de erros"

msgid "Finish booting from a LiveUSB"
msgstr "Terminar o arranque a partir dun USB-executable"

msgid "Finish booting from a LiveUSB or hard drive"
msgstr "Remata o inicio desde un Live USB ou un disco duro"

msgid "Finish booting from a hard drive"
msgstr "Terminar o arranque a partir do disco ríxido"

msgid "Frugal Menus"
msgstr "Menús do Frugal"

msgid "Frugal like p_static_root"
msgstr "Frugal como p_static_root"

msgid "Frugal like persist_all"
msgstr "Frugal como persist_all"

msgid "Frugal like persist_home"
msgstr "Frugal como persist_home"

msgid "Frugal like persist_root"
msgstr "Frugal como persist_root"

msgid "Frugal like persist_static"
msgstr "Frugal como persist_static"

msgid "Frugal menu"
msgstr "Menú Frugal"

msgid "Frugal menus"
msgstr "Menús Frugal"

msgid "GFX menu and GRUB menu"
msgstr "Menú GFX e GRUB"

msgid "GRUB Bootloader"
msgstr "Cargador de arranque GRUB"

msgid "GRUB Menus"
msgstr "Menús GRUB"

msgid "GRUB bootloader"
msgstr "cargador de arranque GRUB"

msgid "GRUB loader"
msgstr "Cargador GRUB"

msgid "GRUB menu"
msgstr "Menú GRUB"

msgid "GRUB menus"
msgstr "Menús GRUB"

msgid "GRUB theme"
msgstr "Tema GRUB"

msgid "GRUB-EFI bootloader"
msgstr "Cargador de arranque GRUB-EFI"

msgid "Hardware clock uses UTC (Linux)"
msgstr "O reloxo do soporte físico usa UTC (Linux)"

msgid "Hardware clock uses local time (Windows)"
msgstr "O reloxo do soporte físico usa a hora local (Windows)"

msgid "Help"
msgstr "Axuda"

msgid "Invert video on some Intel graphics systems"
msgstr "Inverter o vídeo nalgúns isstemas gráficos Intel"

msgid "Kernel options"
msgstr "Opcións do núcleo"

msgid "Keyboard"
msgstr "Teclado"

msgid "Language"
msgstr "Idioma"

msgid "Memory Test"
msgstr "Proba de memoria"

msgid "No EFI bootloader found."
msgstr "Non hai un cargador de arranque EFI."

msgid "No Frugal menu found."
msgstr "Non se atopou un menú Frugal"

msgid "No GRUB CFG-Menu found."
msgstr "Non se atopou ningún menú-CFG de GRUB."

msgid "No GRUB bootloader found."
msgstr "Non se atopou ningún cargador de arranque GRUB."

msgid "No Shim-EFI bootloader found."
msgstr "Non se atopou ningún cargador de arranque Shim-EFI."

msgid "No Windows bootloader found."
msgstr "Non se atopou un cargador de arranque de Windows."

msgid "Only Frugal, no persistence"
msgstr "Só frugal, sen persistencia"

msgid "Only »/home« on persistence device"
msgstr "Só »/home« no dispositivo de persistencia"

msgid "Persistence option"
msgstr "Opción de persistencia"

msgid "Power Off"
msgstr "Apagar"

msgid "Press <Enter> to continue"
msgstr "Prema <Enter> para continuar"

msgid "Press »Enter« to continue"
msgstr "Prema »Enter« para continuar"

msgid "Press »e« to edit, »ESC« to go back."
msgstr "Prema »e« para editar, »ESC« para volver."

msgid "Reboot"
msgstr "Reiniciar"

msgid "Reboot into BIOS/UEFI Setup"
msgstr "Reinicie a configuración da BIOS/UEFI"

msgid "Reset"
msgstr "Restablecer "

msgid "Save options"
msgstr "Gardar opcións"

msgid "Save options (LiveUSB only)"
msgstr "Gardar opcións (só LiveUSB)"

msgid "Save some files across reboots"
msgstr "Garda algúns ficheiros durante os reinicios"

msgid "Secure Boot Restrictions"
msgstr "Restricións de arranque seguro"

msgid "Shim-EFI bootloader"
msgstr "Cargador de arranque Shim-EFI"

msgid "Show text menus"
msgstr "Amosar os textos do menús"

msgid "Show video card detection menu"
msgstr "Amosar menú de detección da tarxeta de vídeo"

msgid "Switch to Syslinux"
msgstr "Cambiar a Syslinux"

msgid "The highlighted entry will start in %d seconds."
msgstr "A entrada realzada comezará en %dsegundos."

msgid "Timezone"
msgstr "Zona horaria"

msgid "UTC or local time is asked"
msgstr "Pregúntase a hora UTC ou local"

msgid "Use ↑ and ↓. Hit »ENTER« to select/deselect."
msgstr "Usa ↑ e ↓. Prema »ENTER« para seleccionar/deseleccionar."

msgid "Windows Bootloader"
msgstr "Cargador de arranque de Windows"

msgid "Windows bootloader"
msgstr "cargador de arranque de Windows"

msgid "Windows bootloader on drive"
msgstr "Cargador de arranque de Windows nunha unidade"

msgid "disabled"
msgstr "desactivado"

msgid "enabled"
msgstr "activado"

msgid "finished"
msgstr "rematado"

msgid "found EFI bootloader at"
msgstr "atopou o cargador de arranque EFI en"

msgid "found Frugal menu at"
msgstr "atopou o menú Frugal en"

msgid "found GRUB bootloader at"
msgstr "atopou o cargador de arranque GRUB en"

msgid "found GRUB menu on"
msgstr "atopou o menú GRUB"

msgid "found Windows bootloader on drive"
msgstr "atopou o cargador de arranque de Windows na unidade"

msgid "root »/« and »/home« in RAM"
msgstr "root »/« e »/home« na memoria RAM"

msgid "root »/« and »/home« separate on persistence device"
msgstr "root »/« e »/home« sepáranse no dispositivo de persistencia"

msgid "root »/« and »/home« together on persistence device"
msgstr "root »/« e »/home« xuntos no dispositivo de persistencia"

msgid "root »/« in RAM, »/home« on persistence device"
msgstr "root »/« na RAM, »/home« no dispositivo de persistencia"

msgid "searching for Frugal grub.entry menus"
msgstr "buscando menús de Frugal grub.entry"

msgid "searching for GRUB bootloader"
msgstr "buscando o cargador de arranque GRUB"

msgid "searching for GRUB menus"
msgstr "buscando os menús GRUB"

msgid "searching for GRUB-EFI bootloader"
msgstr "buscando o cargador de arranque GRUB-EFI"

msgid "searching for Shim-EFI bootloader"
msgstr "buscando o cargador de arranque Shim-EFI"

msgid "searching for Windows bootloader"
msgstr "Buscando o cargador de arranque de Windows"

msgid "version"
msgstr "versión"

msgid "Disable remastering even if linuxfs.new is found."
msgstr "Desactive a remasterización aínda que se atope linuxfs.new."

msgid "Roll back to the previous remastered version."
msgstr "Volve á versión remasterizada anterior."

msgid "Show frugal device selection - can be saved"
msgstr "Amosar unha selección de dispositivos frugal: pódese gardar"

msgid "Show frugal device selection - one time, not saved"
msgstr "Amosar unha selección de dispositivos frugal: unha vez, non gardado"

msgid "found Windows bootloader on"
msgstr "atopou o cargador de arranque de Windows"

msgid ""
"Minimal BASH-like line editing is supported. For the first word, TAB lists "
"possible command completions. Anywhere else TAB lists possible device or "
"file completions. %s"
msgstr ""
"aAdmítese unha liña de edición mínima tipo BASH. Para a primeira palabra, "
"TAB lista as posíbeis ordes de compleción. En calquera outra posición, TAB "
"lista os dispositivos posíbeis ou os ficheiros que completar. %s"

msgid ""
"Minimum Emacs-like screen editing is supported. TAB lists completions. Press"
" Ctrl-x or F10 to boot, Ctrl-c or F2 for a command-line or ESC to discard "
"edits and return to the GRUB menu."
msgstr ""
"É compatíbel co modo mínimo de edición en pantalla estilo Emacs. Completado "
"de listas con TAB. Prema Ctrl-x ou F10 para iniciar, Ctrl-c ou F2 para a "
"liña de ordes ou ESC para descartar cambios e volver ao menú do GRUB."

msgid "ESC at any time exits."
msgstr "ESC en calquera momento para saír."
