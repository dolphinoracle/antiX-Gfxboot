# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# James Bowlin <BitJam@gmail.com>, 2021
# Arnold Marko <arnold.marko@gmail.com>, 2021
# anticapitalista <anticapitalista@riseup.net>, 2021
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-07-04 21:24+0200\n"
"PO-Revision-Date: 2021-07-09 16:40+0000\n"
"Last-Translator: anticapitalista <anticapitalista@riseup.net>, 2021\n"
"Language-Team: Slovenian (https://app.transifex.com/anticapitalista/teams/10162/sl/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: sl\n"
"Plural-Forms: nplurals=4; plural=(n%100==1 ? 0 : n%100==2 ? 1 : n%100==3 || n%100==4 ? 2 : 3);\n"

msgid "Advanced Options"
msgstr "Napredne možnosti"

msgid "Back to main menu"
msgstr "Nazaj na glavni meni"

msgid "Back to main menu (or press »ESC«)"
msgstr "Nazaj na glavni meni (ali pritisnite tipko \"ESC\")"

msgid "Boot Process Performance Visualization"
msgstr "Vizualni prikaz učinkovitosti zagona"

msgid "Boot Rescue Menus"
msgstr "Meniji za reševanje zagona"

msgid "Boot options"
msgstr "Možnosti ob zagonu"

msgid "Change passwords before booting"
msgstr "Spremeni gesla pred zagonom"

msgid "Check LiveUSB and persistence ext2/3/4 file systems"
msgstr "Preveri živi USB in obstojnost ext2/3/4 datotečnega sistema"

msgid "Check integrity of the live media"
msgstr "Preveri integriteto živega nosilca"

msgid "Console options"
msgstr "Možnosti konzole"

msgid "Copy the compressed file system to RAM"
msgstr "Kopiranje stisnjenega datotečnega sistema v pomnilnik RAM"

msgid "Custom"
msgstr "Prilagoditve"

msgid "Customize Boot (text menus)"
msgstr "Prilagojen zagon (besedilni meniji)"

msgid "Desktop options"
msgstr "Možnosti namizja"

msgid "Disable ACPI"
msgstr "Izklop ACPI"

msgid "Disable GRUB theme"
msgstr "Izklop GRUB temo"

msgid "Disable Intel graphics invert"
msgstr "Onemogoči Intelovo inverzijo grafike"

msgid "Disable LiveUSB-Storage feature"
msgstr "Onemogoči funkcijo shrambe za živi USB"

msgid "Disable automount via fstab"
msgstr "Izklop samodejnega priklopa preko fstab"

msgid "Disable console width"
msgstr "Izklopi širino konzole"

msgid "Disable dual video card detection"
msgstr "Izklopi prepoznavanje dvojne grafične kartice"

msgid "Disable swap"
msgstr "Onemogoči swap"

msgid "Disable text-splash screen"
msgstr "Izklopi besedilni zaslon"

msgid "Don't look for USB-2 devices"
msgstr "Brez iskanja USB-2 naprav"

msgid "Don't save files across reboots"
msgstr "Ne shranjuj datotek za naslednje zagone"

msgid "Don't set repositories based on timezone."
msgstr "Ne določaj repozitorijev glede na časovni pas."

msgid "EFI Bootloader"
msgstr "EFI zaganjalnik"

msgid "Enable EFI subsystem for RT-PREEMPT kernels"
msgstr "Vklopi EFI podsisteme za RT-PREEMPT jedra."

msgid "Enable GRUB theme"
msgstr "Vklopi GRUB temo"

msgid "Enable LiveUSB-Storage feature"
msgstr "Vključi shrambo podatkov za živi-usb"

msgid "Enable automount via fstab"
msgstr "Vklop samodejnega priklopa preko fstab"

msgid "Enable dual video card detection"
msgstr "Vklopi prepoznavanje dvojne grafične kartice"

msgid "Enable text-splash screen"
msgstr "Vklopi besedilni zaslon"

msgid "Failsafe options"
msgstr "Možnosti varnega zagona"

msgid "Finish booting from a LiveUSB"
msgstr "Zaključi zagon z živega USB"

msgid "Finish booting from a LiveUSB or hard drive"
msgstr "Zaključi zagon z živega USB ali trdega diska"

msgid "Finish booting from a hard drive"
msgstr "Zaključi zagon s trdega diska"

msgid "Frugal Menus"
msgstr "Frugal meniji"

msgid "Frugal like p_static_root"
msgstr "Frugalu podoben p_static_root"

msgid "Frugal like persist_all"
msgstr "Frugalu podoben persist_all"

msgid "Frugal like persist_home"
msgstr "Frugalu podoben persist_home"

msgid "Frugal like persist_root"
msgstr "Frugalu podoben persist_root"

msgid "Frugal like persist_static"
msgstr "Frugalu podoben persist_static"

msgid "Frugal menu"
msgstr "Frugal meni"

msgid "Frugal menus"
msgstr "Frugal meniji"

msgid "GFX menu and GRUB menu"
msgstr "GFX meni in GRUB meni"

msgid "GRUB Bootloader"
msgstr "GRUB zaganjalnik"

msgid "GRUB Menus"
msgstr "GRUB meniji"

msgid "GRUB bootloader"
msgstr "GRUB zaganjalnik"

msgid "GRUB loader"
msgstr "¸GRUB nalagalnik"

msgid "GRUB menu"
msgstr "GRUB meni"

msgid "GRUB menus"
msgstr "GRUB meniji"

msgid "GRUB theme"
msgstr "GRUB tema"

msgid "GRUB-EFI bootloader"
msgstr "GRUB-EFI zaganjalnik"

msgid "Hardware clock uses UTC (Linux)"
msgstr "Strojna ura uporablja UTC (Linux)"

msgid "Hardware clock uses local time (Windows)"
msgstr "Strojna ura uporablja lokalni čas (Windows)"

msgid "Help"
msgstr "Pomoč"

msgid "Invert video on some Intel graphics systems"
msgstr "Invertiraj video na nekaterih Intelovih grafičnih sistemih"

msgid "Kernel options"
msgstr "Možnosti jedra"

msgid "Keyboard"
msgstr "Tipkovnica"

msgid "Language"
msgstr "Jezik"

msgid "Memory Test"
msgstr "Testiranje pomnilnika"

msgid "No EFI bootloader found."
msgstr "EFI zaganjalnik ni bil najden."

msgid "No Frugal menu found."
msgstr "Frugal meni ni bil najden."

msgid "No GRUB CFG-Menu found."
msgstr "GRUB CFG-Meni ni bil najden."

msgid "No GRUB bootloader found."
msgstr "GRUB zaganjalnik ni bil najden."

msgid "No Shim-EFI bootloader found."
msgstr "Shim-EFI zaganjalnik ni bil najden."

msgid "No Windows bootloader found."
msgstr "Windows zaganjalnik ni bil najden."

msgid "Only Frugal, no persistence"
msgstr "Zgolj varčna, brez obstojnosti"

msgid "Only »/home« on persistence device"
msgstr "Zgolj \"/home\" na obstojno napravo"

msgid "Persistence option"
msgstr "Možnosti obstojnosti"

msgid "Power Off"
msgstr "Izklop sistema"

msgid "Press <Enter> to continue"
msgstr "Pritisnite <Enter> za nadaljevanje"

msgid "Press »Enter« to continue"
msgstr "Pritisnite »Enter« za nadaljevanje"

msgid "Press »e« to edit, »ESC« to go back."
msgstr "Pritinite \"e\" za urejanje ali \"ESC\" za povratek."

msgid "Reboot"
msgstr "Ponovni zagon"

msgid "Reboot into BIOS/UEFI Setup"
msgstr "Ponovni zagon v BIOS/UEFI nastavitve"

msgid "Reset"
msgstr "Ponastavitev"

msgid "Save options"
msgstr "Shrani možnosti"

msgid "Save options (LiveUSB only)"
msgstr "Shrani možnosti (zgolj živi-usb)"

msgid "Save some files across reboots"
msgstr "Shrani nekatere datoteke za naslednje zagone"

msgid "Secure Boot Restrictions"
msgstr "Omejitve varnega zagona"

msgid "Shim-EFI bootloader"
msgstr "Shim-EFI zaganjalnik"

msgid "Show text menus"
msgstr "Prikaži besedilne menije"

msgid "Show video card detection menu"
msgstr "Prikaži meni za prepoznavanje grafične kartice"

msgid "Switch to Syslinux"
msgstr "Preklopi na Syslinux"

msgid "The highlighted entry will start in %d seconds."
msgstr "Osvetljeni vnos bo zagnan v %d sekundah."

msgid "Timezone"
msgstr "Časovni pas"

msgid "UTC or local time is asked"
msgstr "Zahtevan je UTC ali lokalni čas"

msgid "Use ↑ and ↓. Hit »ENTER« to select/deselect."
msgstr "Uporabite ↑ in ↓. Pritisnite »ENTER« za izbiro ali ne-izbiro."

msgid "Windows Bootloader"
msgstr "Windows zaganjalnik"

msgid "Windows bootloader"
msgstr "Windows zaganjalnik"

msgid "Windows bootloader on drive"
msgstr "Windows zaganjalnik na pogonu"

msgid "disabled"
msgstr "onemogočeno"

msgid "enabled"
msgstr "omogočeno"

msgid "finished"
msgstr "končano"

msgid "found EFI bootloader at"
msgstr "našel EFI zaganjalnik na"

msgid "found Frugal menu at"
msgstr "našel Frugal meni na"

msgid "found GRUB bootloader at"
msgstr "našel GRUB zaganjalnik na"

msgid "found GRUB menu on"
msgstr "našel GRUB meni na"

msgid "found Windows bootloader on drive"
msgstr "našel EFI zaganjalnik na pogonu"

msgid "root »/« and »/home« in RAM"
msgstr "korenski »/« in domači »/home« v pomnilniku"

msgid "root »/« and »/home« separate on persistence device"
msgstr "korenski »/« in domači »/home« ločeno na obstojnem pogonu"

msgid "root »/« and »/home« together on persistence device"
msgstr "korenski »/« in domači »/home« skupaj na obstojnem pogonu"

msgid "root »/« in RAM, »/home« on persistence device"
msgstr "korenski »/« v pomnilniku, domači »/home« na obstojnem pogonu"

msgid "searching for Frugal grub.entry menus"
msgstr "iskanje Frugal grub.entry menijev"

msgid "searching for GRUB bootloader"
msgstr "iskanje GRUB zaganjalnika"

msgid "searching for GRUB menus"
msgstr "iskanje GRUB menijev"

msgid "searching for GRUB-EFI bootloader"
msgstr "iskanje GRUB-EFI zaganjalnika"

msgid "searching for Shim-EFI bootloader"
msgstr "iskanje Shim-EFI zaganjalnika"

msgid "searching for Windows bootloader"
msgstr "iskanje Windows zaganjalnika"

msgid "version"
msgstr "različica"

msgid "Disable remastering even if linuxfs.new is found."
msgstr "Izklopi remasterizacijo, čeprav je bil najden nov linuxfs.new"

msgid "Roll back to the previous remastered version."
msgstr "Prevji nazaj na prejšnjo remasterizirano različico."

msgid "Show frugal device selection - can be saved"
msgstr "Prikaži izbor Frugal naprav - lahko se shrani"

msgid "Show frugal device selection - one time, not saved"
msgstr "Prikaži izbor Frugal naprav - enkrat, brez shranjevanja"

msgid "found Windows bootloader on"
msgstr "našel Windows zaganjalnik na"

msgid ""
"Minimal BASH-like line editing is supported. For the first word, TAB lists "
"possible command completions. Anywhere else TAB lists possible device or "
"file completions. %s"
msgstr ""
"Podprt je enostaven način urejanja, podoben BASH. Za prvo besedo TABULATOR "
"izpiše mogoča dokončanja ukazov. Povsod drugod TABULATOR našteje mogoča "
"dokončanja naprav ali datotek. %s"

msgid ""
"Minimum Emacs-like screen editing is supported. TAB lists completions. Press"
" Ctrl-x or F10 to boot, Ctrl-c or F2 for a command-line or ESC to discard "
"edits and return to the GRUB menu."
msgstr ""
"Podprt je enostaven način urejanja, podoben Emacs. Tipka TAB izpiše možnosti"
" dopolnjevanja. Pritisnite tipki Ctrl-x ali F10 za zagon, Ctrl-c ali F2 za "
"ukazno vrstico ali ESC za zavračanje urejanja in vrnitev v meni GRUB."

msgid "ESC at any time exits."
msgstr "ESC kadarkoli konča program."
