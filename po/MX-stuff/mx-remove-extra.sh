#!/bin/bash

# mx-remove-extra.sh for MX
# * to remove addtional msgid strings found in mxextra.pot from bootloader.pot
# * needs to be located with the some diretoy as mxextra.pot

ME=${BASH_SOURCE[0]}
RP=$(realpath $(dirname "$ME"))


BL_DIR=$(realpath "$RP"/..)
PO_DIR=$BL_DIR/po

BL_POT="$BL_DIR/bootloader.pot"
XT_POT="$RP/mxextra.pot"

[ -f "$XT_POT" ] || exit 0
[ -f "$BL_POT" ] || exit 0
[ -d "$PO_DIR" ] || exit 0

# check anything to remove
MSG_COMM=$(msgcomm "$BL_POT" "$XT_POT" | grep -cw ^msgid)
((MSG_COMM==0)) && exit

mv $BL_POT $BL_POT~
msgcomm --no-wrap -u $BL_POT~ $XT_POT > $BL_POT
rm $BL_POT~
for po in $PO_DIR/*.po; do
    [ -f "$po" ] || continue
    msgmerge -v --backup=none --no-wrap -U -N $po  $BL_POT
    sed -i '/^#~ /d' $po
done

# fix po/pot header
# * add one space after "#" empty line
# * move Language: line after Content-Transfer line
# * remove empty last line
for po in $PO_DIR/*.po $BL_POT; do
    [ -f "$po" ] || continue
	sed -i~  '
		/^#\s*$/s//# /;
		/^"Language:/{x; /^$/{x;h;d};  /^.$/d }; 
		/"Content-Transfer/{x; /^"Language:/{H;x;p;d}; d};
		${/^$/d};
		' $po
done
