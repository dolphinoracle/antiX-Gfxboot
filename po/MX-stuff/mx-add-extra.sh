#!/bin/bash

# mx-add-extra.sh for MX
# * to add msgid strings found in mxextra.pot into bootloader.pot
# * needs to be located within the same directory as mxextra.pot
# * updates available translations from grub's po files

ME=${BASH_SOURCE[0]}
RP=$(realpath $(dirname "$ME"))

BL_DIR=$(realpath "$RP"/..)
PO_DIR=$BL_DIR/po
TX_DIR=$BL_DIR/grub/tx

BL_POT="$BL_DIR/bootloader.pot"
BK_POT="$BL_POT".BK~
XT_POT="$RP/mxextra.pot"

# check anything todo
[ -f "$XT_POT" ] || exit 0
[ -f "$BL_POT" ] || exit 0
[ -d "$PO_DIR" ] || exit 0
[ -d "$TX_DIR" ] || exit 0

# check for new msgid's to add
MSG_COMM=$(msgcomm "$BL_POT" "$XT_POT" | grep -cw ^msgid)
((MSG_COMM==0)) || exit

# merge extra pot into existing bootloader pot
mv "$BL_POT" "$BK_POT"
msgcomm --no-wrap -u "$BK_POT" "$XT_POT" > "$BL_POT"
rm "$BK_POT"

# update po's with translations from grub-po's
for po in $PO_DIR/*.po; do
    [ -f "$po" ] || continue
    msgmerge -v --backup=none --no-wrap -U -N $po  $BL_POT
	pn=${po##*/}
    gpo=$TX_DIR/$pn
    [ -f $gpo ] || continue
    npo=${gpo%.po}.gpo
    msgcomm --no-wrap "$gpo" "$XT_POT" > "$npo"
    msgmerge -v --no-wrap --backup=none --no-wrap -U -N -C $npo $po  $BL_POT
    rm $npo
done

[ -f $PO_DIR/en.po ] || exit 0
# special handling of english "translations" in pot-file and en.po
EN_POT="$RP/mxextra_en.pot"
sed '/^msgstr/d; /^msgid/{p;s/^msgid/msgstr/}' $XT_POT > $EN_POT

cp $BL_POT $BL_POT~
msgmerge -v --no-wrap --backup=none -U -N -C $EN_POT $BL_POT  $BL_POT~
msgmerge -v --no-wrap --backup=none -U -N -C $EN_POT $PO_DIR/en.po  $BL_POT
rm $EN_POT $BL_POT~


# fix po/pot header
# * add one space after "#" empty line
# * move Language: line after Content-Transfer line
# * remove empty last line
for po in $PO_DIR/*.po $BL_POT; do
    [ -f "$po" ] || continue
	sed -i~  '
		/^#\s*$/s//# /;
		/^"Language:/{x; /^$/{x;h;d};  /^.$/d }; 
		/"Content-Transfer/{x; /^"Language:/{H;x;p;d}; d};
		${/^$/d};
		' $po
done
